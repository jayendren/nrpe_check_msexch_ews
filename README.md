﻿# About

Nagios IMAP plugin from https://exchange.nagios.org/directory/Plugins/Network-Protocols/IMAP4-and-POP3/sgichk_imap_inboxcount/details has issues with MS EXCH 2010; this plugin uses MS EXCH EWS to login to the mailbox and count messages.

Follows standard nrpe exit codes based on thresholds:

```
nrpe_ok_exit_code      = 0
nrpe_warn_exit_code    = 1
nrpe_crit_exit_code    = 2
nrpe_unknown_exit_code = 3
```

# Requirements

* Ruby 2.x
* viewpoint gem - https://github.com/WinRb/Viewpoint

```
gem install viewpoint --no-ri --no-rdoc
```

# Usage

```
$ ./check_msexch_ews.rb   
Missing options: warning, critical, ok, user, pass, email, endpoint
Usage: check_msexch_ews [options]
    -w, --warn Integer               warning message count
    -c, --crit Integer               critical message count
    -o, --ok Integer                 ok message count
    -u, --user String                mailbox user name
    -p, --passwd String              mailbox user password
        --email String               mailbox email address
    -e, --endpoint String            EWS endpoint
    -h, --help                       Help
```

# Example

```
$ ./check_msexch_ews.rb \
--warn 2 --crit 3 --ok 1 \
--user 'NOOP\svc_test' \
--passwd 'P@5sw0rd' \
--email 'test13@noop' \
--endpoint 'https://webmail.noop/ews/Exchange.asmx'
CRITICAL::MAILBOX::test13@noop::MSG_COUNT::19
$ ~/gitwork/pbc_check_msexch_ews » echo $?                                                                                                                            
2
$ ~/gitwork/pbc_check_msexch_ews » 
```
