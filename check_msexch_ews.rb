#!/usr/bin/env ruby

require 'rubygems'
require 'viewpoint'
require 'optparse'
include Viewpoint::EWS

# global variables #
@msgcount       = 0
# nrpe exit codes #
@nrpe_ok_exit_code      = 0
@nrpe_warn_exit_code    = 1
@nrpe_crit_exit_code    = 2
@nrpe_unknown_exit_code = 3

# MAIN #

begin

  options         = {}
  optparse = OptionParser.new do |opts|
    opts.on('-w', '--warn Integer', 'warning message count') do |warn_msg_count|
      options[:warning] = warn_msg_count.to_i
    end

    opts.on('-c', '--crit Integer', 'critical message count') do |crit_msg_count|
      options[:critical] = crit_msg_count.to_i
    end

    opts.on('-o', '--ok Integer', 'ok message count') do |ok_msg_count|
      options[:ok] = ok_msg_count.to_i
    end

    opts.on('-u', '--user String', 'mailbox user name') do |mbox_user|
      options[:user] = mbox_user.to_s
    end

    opts.on('-p', '--passwd String', 'mailbox user password') do |mbox_pass|
      options[:pass] = mbox_pass.to_s
    end

    opts.on('-e', '--email String', 'mailbox email address') do |email|
      options[:email] = email.to_s
    end

    opts.on('-endpoint', '--endpoint String', 'EWS endpoint') do |endpoint|
      options[:endpoint] = endpoint.to_s
    end    

    opts.on('-h', '--help', 'Help') do
      puts opts
      puts "Example: #{$0} --warn 2 --crit 3 --ok 1 --user 'DOMAIN\\username' --passwd 'passwd' --email 'user@domain' --endpoint 'https://exchange.domain/ews/Exchange.asmx'"
      exit
    end
  end

  optparse.parse!
  mandatory = [:warning, :critical, :ok, :user, :pass, :email, :endpoint]   
  missing = mandatory.select{ |param| options[param].nil? }
  unless missing.empty?
    puts "Missing options: #{missing.join(', ')}"
    puts optparse
    exit
  else
    begin
      client = Viewpoint::EWSClient.new(options[:endpoint], options[:user], options[:pass], http_opts: {ssl_verify_mode: 0})
      inbox = client.get_folder(:inbox)

      inbox.items.each {|item|
        @msgcount = @msgcount + 1
        puts item.inspect
      }
    rescue
      printf "UNKNOWN: Viewpoint::EWSClient.new failed - check username, password or network connectivity to %s \n" % [options[:endpoint]]
      Process.exit(@nrpe_unknown_exit_code)
    end

    if (@msgcount >= options[:warning]) and (@msgcount <= options[:critical])
      printf "WARNING::MAILBOX::%s::MSG_COUNT::%s\n"  % [options[:email], @msgcount]
      Process.exit(@nrpe_warn_exit_code)
    elsif @msgcount >= options[:critical]
      printf "CRITICAL::MAILBOX::%s::MSG_COUNT::%s\n"  % [options[:email], @msgcount]
      Process.exit(@nrpe_crit_exit_code)
    elsif @msgcount <= options[:ok]
      printf "OK::MAILBOX::%s::MSG_COUNT::%s\n"  % [options[:email], @msgcount]
      Process.exit(@nrpe_ok_exit_code)
    else
      printf "UNKNOWN::MAILBOX::%s::MSG_COUNT::%s\n"  % [options[:email], @msgcount]
      Process.exit(@nrpe_unknown_exit_code)
    end
  end          

rescue OptionParser::InvalidOption, OptionParser::MissingArgument
  puts $!.to_s
  puts optparse
  exit
end  